﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using PdfGenerator.Services;

namespace PdfGenerator
{
    public class BrowserConnectHealthCheck : IHealthCheck
    {
        private readonly IBrowserProvider _browserProvider;

        public BrowserConnectHealthCheck(IBrowserProvider browserProvider)
        {
            _browserProvider = browserProvider;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var browser = await _browserProvider.GetBrowserAsync();
                return await Task.FromResult(HealthCheckResult.Healthy("Browser connection OK"));
            }
            catch (Exception ex)
            {
                return await Task.FromResult(new HealthCheckResult(context.Registration.FailureStatus, $"Unable to connect to browser {ex.Message}"));
            }
        }
    }
}