param appName string
param location string
@secure()
param dockerRegistryPassword string
param registryName string
param dockerRegistryUserName string
param aspName string
param rgSharedResources string
param dockerImageName string
param dockerImageTag string
param vnetName string
param subnetName string
param privateDnsZoneName string = 'privatelink.azurewebsites.net'

resource appServicePlan 'Microsoft.Web/serverfarms@2020-12-01' existing = {
  name: aspName
  scope: resourceGroup(rgSharedResources)
}

resource webApp 'Microsoft.Web/sites@2021-01-01' = {
  name: appName
  location: location
  identity: {
    type: 'SystemAssigned'
  }
  tags: {}
  properties: {
    serverFarmId: appServicePlan.id
    httpsOnly: true
    clientAffinityEnabled: false
    siteConfig: {
      appSettings: [ 
        {
          name: 'DOCKER_REGISTRY_SERVER_PASSWORD'
          value: dockerRegistryPassword
        }
        {
          name: 'DOCKER_REGISTRY_SERVER_URL'
          value: '${registryName}.azurecr.io'
        }
        {
          name: 'DOCKER_REGISTRY_SERVER_USERNAME'
          value: dockerRegistryUserName
        }]
      linuxFxVersion: 'DOCKER|${registryName}.azurecr.io/${dockerImageName}:${dockerImageTag}'
    }
    
  }
}
var privateEndpointName = 'pe-${appName}'

resource privateEndpoint 'Microsoft.Network/privateEndpoints@2023-05-01' = {
  name: privateEndpointName
  location: location
  properties: {
    subnet: {
      id: resourceId(rgSharedResources,'Microsoft.Network/virtualNetworks/subnets', vnetName, subnetName)
    }
    privateLinkServiceConnections: [
      {
        name: privateEndpointName
        properties: {
          groupIds: ['sites']
          privateLinkServiceId: webApp.id
        }
      }
    ]
  }
}

module addToPrivateDns 'AddToPrivateDns.bicep' = {
  name: 'addToPrivateDns'
  params: {
    privateDnsZoneName: privateDnsZoneName
    privateEndpointName: privateEndpointName
    appResourceGroupName: resourceGroup().name
    appName: appName
  }
  dependsOn: [privateEndpoint]
  scope: resourceGroup(rgSharedResources)
}
